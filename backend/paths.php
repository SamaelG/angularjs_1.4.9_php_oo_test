<?php
//SITE_ROOT
$path = $_SERVER['DOCUMENT_ROOT'] . '/accelivia-master/Angular/php/backend/';
define('SITE_ROOT', $path);
define('SITE_PATH', "http://".$_SERVER['HTTP_HOST'] . '/accelivia-master/Angular/php/backend/');
define('Amigable_PATH', "http://".$_SERVER['HTTP_HOST'] . '/accelivia-master/Angular/php/backend');

//utils Path
    define('Utils_ROOT', SITE_ROOT."utils/");

//classes Path
    define("Classes_ROOT",SITE_ROOT."classes/");
        define("BLL_ROOT",Classes_ROOT."bd/BLLs/");
        define("CONF_ROOT",Classes_ROOT."bd/conf_and_connection/");
        define("DAO_ROOT",Classes_ROOT."bd/DAO/");
        define("Classes_Utils_ROOT",Classes_ROOT."bd/utils/");
//Controllers
    define("Controller_ROOT",SITE_ROOT."controller/");
        define("Controller_Home",Controller_ROOT."home_controller/");

 define('URL_AMIGABLES', TRUE);