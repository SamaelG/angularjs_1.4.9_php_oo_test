<?php
error_reporting( E_ALL );
require_once('paths.php');
require_once(Classes_Utils_ROOT.'load_bll.inc.php');
/* require_once('classes/Router.php'); */

require_once(Utils_ROOT.'amigable.inc.php');

function require_path($Path,$className,$extension){
   if (file_exists($Path . $className . $extension)) {//require(BLL_USERS . "user_bll.class.singleton.php");
        require($Path. $className . $extension);
        set_include_path($Path);
        spl_autoload($className);
    }
} 


  spl_autoload_register(function($class_name)
  {
    require_path(Classes_ROOT,$class_name,'.class.singletone.php');
    
    /* BD Clases */
        require_path(BLL_ROOT,$class_name,'.class.singletone.php');
        require_path(CONF_ROOT,$class_name,'.class.singletone.php');
        require_path(Classes_Utils_ROOT,$class_name,'.php');
        require_path(DAO_ROOT,$class_name,'.class.singletone.php');
    /* BD Classes */


    require_path(Controller_ROOT,$class_name,'.php');
        require_path(Controller_Home,$class_name,'.php');
        
    require_path(Utils_ROOT,$class_name,'.php');


  });


  Router::run();
 ?>