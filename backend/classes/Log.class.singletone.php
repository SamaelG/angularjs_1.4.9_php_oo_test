<?php
class Log {
    private $_path="";
    private $_fileName="";
    private $_long_path="";
    static $_instance;

    public function __construct() {
        $this->_path = "log/";
        $this->_fileName = 'myLog.log';
        $this->_long_path = $this->_path.$this->_fileName;

    }
    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    protected function _save($line) {
        $fhandle = fopen($this->_long_path, "a+");
        fwrite($fhandle, $line);
        fclose($fhandle);
    }

    public function addLine($line){
        $line = is_array($line) ? print_r($line, true) : $line;
        $line = date("d-m-Y h:i:s") . ": $line\n";
        $this->_save($line);
    }

}

?>