<?php
class common_sentence {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = DAO::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function simple_update($tabla, $where, $all_content_array) {
        $this->dao->update($tabla);
        foreach ($all_content_array as $key => $value){
            if ($key == 0) {
                $this->dao->update_set($value["column"], $value["valor"]);
            }else{
                $this->dao->more_update_set($value["column"], $value["valor"]);
            }
        }
        $this->dao->where_argument($where);
        $this->dao->put($this->db,$this->dao->content);
        var_dump($this->dao->content);
    }
}

?>