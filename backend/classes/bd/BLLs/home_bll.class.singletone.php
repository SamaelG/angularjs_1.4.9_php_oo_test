<?php
class home_bll extends core_bll {
    static $_instance;

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function simple_select(){
        try {
            if (!method_exists("DAO", "select")){
                throw new myException("The Method in DAO doesn't exist");
            }
            $dao_instance=$this->dao;
/*             $dao_instance->insert("test_insert");
            $dao_instance->firstvalue("Valor 1");
            $dao_instance->middlevalues("Valor 2, Valor 3");
            $dao_instance->closeinsertstatement();
            $dao_instance->put($this->db,$dao_instance->content); */

            $contents_array = array(array("column"=>"texto1","valor"=>"'Texto modificado'"));
            $this->common->simple_update("test_select", "texto1='Este es el texto1'", $contents_array);

            $dao_instance->select("*","test_select");
            // return $dao_instance->content;
            return $dao_instance->get($this->db,$dao_instance->content);
        } catch (myException $e) {
            $e->get_Message();
            return $e->show_simple_error();
        };
    }
}

?>