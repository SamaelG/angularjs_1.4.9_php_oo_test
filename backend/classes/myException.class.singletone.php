<?php
class myException extends Exception {
    function get_Message() {
        $menssage=$this->getMessage();
        $code=$this->getCode();
        $file=$this->getFile();
        $at_line=$this->getLine();
        $with_trace=$this->getTraceAsString();
        $msg = "Exception has ocurred: $menssage".
                "File affected: $file".
                "At line: $at_line".
                "With trace: $with_trace";
        $this->save_in_log($msg);
            return $msg;
    }

    function show_simple_error() {
        $menssage=$this->getMessage();
        $msg = "Exception has ocurred: $menssage";
        return $msg;
    }

    function save_in_log($msg) {
        $log = Log::getInstance();
        $log->addLine($msg);
    }
}

?>