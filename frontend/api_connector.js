app.factory("services", ['$http','$q', function ($http, $q) {
    var serviceBase = "backend/index.php?module=";
    var obj = {};

        obj.get = function () {
            var url=serviceBase ;
            var method='GET';
            return http_1(method,url,$http,$q);
        };

        obj.post = function (clase, method, datos) {
            /* console.log(dada); */
            var metodo= 'POST';
            var url= serviceBase + clase + "&function=" + method;
            var data= datos;
            return  http_2(metodo,url,data,$http,$q);
        };

        
    return obj;
}]);
function http_1(method,url,$http,$q){
    var defered=$q.defer();
    var promise=defered.promise;
    $http({
            method: method ,
            url: url
        }).success(function(data, status, headers, config) {
            defered.resolve(data);
        }).error(function(data, status, headers, config) {
            defered.reject(data);
        });
    return promise;

}
function http_2(method,url,data,$http,$q){
    var defered=$q.defer();
    var promise=defered.promise;
    $http({
            method: method ,
            url: url,
            data: data
        }).success(function(data, status, headers, config) {
            defered.resolve(data);
        }).error(function(data, status, headers, config) {
            defered.reject(data);
        });
        /* console.log(data); */
    return promise;

}