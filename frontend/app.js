var app = angular.module('angularBasics', ["ngRoute",'ngMaterial', 'ngMessages']);


////Router
    app.config(['$routeProvider',function ($routeProvider) {
        $routeProvider
            .when("/", {
                controller: "api_controller",
                controllerAs: "api_controller",
                templateUrl: "frontend/modules/view/api_controller.view.html"
            })
    }]);
////Router 